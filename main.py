import paho.mqtt.client as mqtt
import json
import time
import os
import argparse
import subprocess
from snowflake import SnowflakeGenerator


processes = {}

parser = argparse.ArgumentParser(
    description="Execute the Brian component, responsible for the creation of Adam instances."
)
parser.add_argument("--mqtt_broker_address", help="Address of the MQTT broker", default="127.0.0.1")
parser.add_argument("--margot_heel_generator_DIR", help="Directory of installation of mARGOt, relative to brian folder", default="../core/install/lib/cmake/margot/")
parser.add_argument("--nlohmann_json_DIR", help="Directory of installation of nlohmann_json, relative to brian folder", default="../json/install/lib/cmake/nlohmann_json/")
parser.add_argument("--eclipse_paho_mqtt_c_DIR", help="Directory of installation of eclipse-paho-mqtt-c, relative to brian folder", default="../paho.mqtt.c/install/lib/cmake/eclipse-paho-mqtt-c/")

args = parser.parse_args()

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("adam/+/request")
    client.subscribe("adam/+/stop")
    client.subscribe("brian/+/get_id")
    client.subscribe("brian/prepare")

def generate_adam(exchanged, snow_id):
    pwd = os.getcwd()
    process = subprocess.Popen(["mkdir " + snow_id + " && cp -r ./adam/* ./" + snow_id + "/"], shell=True)
    process.wait()
    out, err = process.communicate()
    if(err):
        print("Error generating folder " + snow_id)
    with open(snow_id + "/margot.json", "w") as f:
        json.dump(exchanged, f)
    with open(snow_id + "/CMakeLists.txt", "w") as dest, open(snow_id + "/CMakeLists_original.txt", "r") as source:
        for line in source:
            fixed = line.replace("<path_of_margot_config_file>", pwd + "/" + snow_id + "/margot.json")
            dest.writelines([fixed])
    process = subprocess.Popen([
        "cd " + snow_id + " && " +
        "mkdir build && " +
        "cd build && " +
        "cmake " + 
            "-Dmargot_heel_generator_DIR="+pwd+"/"+args.margot_heel_generator_DIR + " " +
            "-Dnlohmann_json_DIR="+pwd+"/"+args.nlohmann_json_DIR + " " +
            "-Declipse-paho-mqtt-c_DIR="+pwd+"/"+ args.eclipse_paho_mqtt_c_DIR + " " + 
            ".. &&" +
        "make"], shell=True)
    process.wait()
    out, err = process.communicate()
    if(err):
        print(err)
    return subprocess.Popen(["cd " + snow_id + "/build && ./adam " + snow_id], shell=True)


def on_message(client, userdata, msg):
    if "get_id" in msg.topic:
        exchanged = json.loads(msg.payload.decode("utf-8"))
        t_id = msg.topic.split("/")[1]
        name = exchanged["name"]
        version = exchanged["version"]
        block = exchanged["blocks"][0]["name"]
        app_id = name + "^" + version + "^" + block
        available = [x for x in processes if not processes[x]["paired"] and processes[x]["app_id"] == app_id]
        snow_id = str(next(SnowflakeGenerator(69)))
        while snow_id in processes:
            snow_id = str(next(SnowflakeGenerator(69)))
        if len(available) == 0:
            to_add = {}
            to_add["process"] = generate_adam(exchanged, snow_id)
            to_add["app_id"] = app_id
            to_add["paired"] = True
            processes[snow_id] = to_add
            print("Adam with ID " + snow_id + " launched!")
        else:
            snow_id = available[0]
            processes[snow_id]["paired"] = True
        client.publish("brian/" + t_id + "/app_id", snow_id) 
    
    if "prepare" in msg.topic:
        exchanged = json.loads(msg.payload.decode("utf-8"))
        name = exchanged["name"]
        version = exchanged["version"]
        block = exchanged["blocks"][0]["name"]
        app_id = name + "^" + version + "^" + block
        snow_id = str(next(SnowflakeGenerator(69)))
        while snow_id in processes:
            snow_id = str(next(SnowflakeGenerator(69)))
        to_add = {}
        to_add["process"] = generate_adam(exchanged, snow_id)
        to_add["app_id"] = app_id
        to_add["paired"] = False
        processes[snow_id] = to_add
        print("Adam with ID " + snow_id + " prepared!")

    if "request" in msg.topic:
        exchanged = json.loads(msg.payload.decode("utf-8"))
        name = exchanged["name"]
        version = exchanged["version"]
        block = exchanged["blocks"][0]["name"]
        app_id = name + "^" + version + "^" + block
        snow_id = msg.topic.split("/")[1]
        to_add = {}
        to_add["process"] = generate_adam(exchanged, snow_id)
        to_add["app_id"] = app_id
        to_add["paired"] = True
        processes[snow_id] = to_add
        print("Adam with ID " + snow_id + " running!")
        #client.publish("margot/" + new_app_id + "/welcome/" + t_id, json.dumps(exchanged))
        
    elif "stop" in msg.topic:
        snow_id = msg.topic.split("/")[1]
        processes[snow_id]["process"].kill()
        for item in processes[snow_id]:
            del item
        del processes[snow_id]
        process = subprocess.Popen(["rm", "-r", "-f", "./" + snow_id])
        process.wait()
        out, err = process.communicate()
        if(err):
            print("Error destroying folder " + snow_id)
        print("Adam with ID " + snow_id + " killed!")

if(not os.path.exists("./adam/README.md")):
    process = subprocess.Popen(["git clone https://gitlab.com/margot_project/adam"], shell=True)
    process.wait()
    out, err = process.communicate()
    if(err):
        print("Error cloning adam, aborting")
        exit(1)

print("Brian started, waiting for connections...")

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect(args.mqtt_broker_address, 1883, 60)

client.loop_forever()

