# Brian

Brian (BRoker for Initialization of Adam components on a remote Node) is an auxiliary component that automates the creation of Adam instances. It can interact with applications through MQTT to start Adam instances for them.

## Interface definition

The communication between Brian and the application is performed over MQTT and uses the following topics:

* adam/[adam_id]/request: it is used by the application to request for an Adam instance. The message must contain a valid margot configuration file that will be used during the generation of the Adam component. After the creation of the Adam instance, the application will be able to interact directly with it.
* adam/[adam_id]/stop: it is used by the application to notify Brian about the termination of the execution. The content of the message is irrelevant. After receiving this message, Brian will dismantle the instance created before.

All these topics contain [adam_id], which is a number used to identify different Adam instances. 

## Installation

* Python3.8 required;
* Create the virtual environment with "virtualenv ./env"
* Load the environment with "source env/bin/activate"
* Install the requirements with "pip3 install -r requirements.txt"

## Execution instructions

```
python3 main.py
```

## Optional command line parameters

* --mqtt_broker_address: The address of the MQTT broker. default=127.0.0.1
* --margot_heel_generator_DIR: Directory of installation of mARGOt, relative to brian folder. default="../core/install/lib/cmake/margot/"
* --nlohmann_json_DIR: Directory of installation of nlohmann_json, relative to brian folder. default="../json/install/lib/cmake/nlohmann_json/"
* --eclipse_paho_mqtt_c_DIR: Directory of installation of eclipse-paho-mqtt-c, relative to brian folder. default="../paho.mqtt.c/install/lib/cmake/eclipse-paho-mqtt-c/"

### Acknowledgment

This work has been supported by European Commission under the following grants:

* EVEREST No. 957269 (dEsign enVironmEnt foR Extreme-Scale big data analyTics on heterogeneous platforms)
