FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y && apt-get install -y build-essential git

RUN apt-get update -y && apt-get install -y python3 python3-dev python3-pip

RUN apt-get update && apt-get install -y cmake wget libgtest-dev libssl-dev && rm -rf /var/lib/apt/lists/*

RUN cd /home && wget http://downloads.sourceforge.net/project/boost/boost/1.60.0/boost_1_60_0.tar.gz \
  && tar xfz boost_1_60_0.tar.gz \
  && rm boost_1_60_0.tar.gz \
  && cd boost_1_60_0 \
  && ./bootstrap.sh --with-libraries=program_options \
  && ./b2 install \
  && cd /home \
  && rm -rf boost_1_60_0

ENV LD_LIBRARY_PATH=/usr/local/lib
ENV margot_heel_generator_DIR=/usr/local/lib/cmake/margot

WORKDIR /usr/src/margot
RUN cd /usr/src/margot \
  && git clone https://github.com/eclipse/paho.mqtt.c \
  && cd paho.mqtt.c \ 
  && mkdir build \
  && cd build \
  && cmake .. \
  && make install \
  && cd /usr/src/margot  

ADD https://gitlab.com/api/v4/projects/2757421/repository/branches/master version.json
RUN cd /usr/src/margot \
  && git clone https://gitlab.com/margot_project/core.git \
  && cd core \
  && git submodule init \
  && git submodule update \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make install \
  && cd ../..

RUN cd /usr/src/margot \
  && git clone https://github.com/nlohmann/json.git \
  && cd json \
  && mkdir build \
  && cd build \
  && cmake .. \
  && make install \
  && cd ../..

RUN cd /usr/src/margot \
  && git clone https://gitlab.com/margot_project/brian.git \
  && cd brian \
  && pip3 install -r requirements.txt

ADD https://gitlab.com/api/v4/projects/34540210/repository/branches/master version.json
RUN cd /usr/src/margot/brian \
  && git clone https://gitlab.com/margot_project/adam.git

WORKDIR /usr/src/margot/brian
ENTRYPOINT ["python3", "main.py"]
CMD ["--mqtt_broker_address", "127.0.0.1"]